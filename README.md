# Google Hire

[Google Hire](https://hire.google.com/) is an applicant tracking and recruiting
system. This module provides integration between Drupal and the Google Hire API.

* For a full description of the module, visit the [project page](https://drupal.org/project/google_hire).
* To submit bug reports and feature suggestions, or to track changes, refer to
  the [issue queue](https://drupal.org/project/issues/google_hire).

## Installation

Installation via Composer is recommended but not required.

`composer require drupal/google_hire`

## Configuration

* Configure the domain associated with Google Hire at `admin/config/services/google_hire`.
* Assign the appropriate Drupal permissions for Google Hire.
* Published listings should display at `/careers`.

## Maintainers

Current maintainers:

* Mark Dorison ([markdorison](https://www.drupal.org/u/markdorison))

This project has been sponsored by [Chromatic](https://chromatichq.com).
